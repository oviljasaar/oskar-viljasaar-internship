From 361b792ffc1596935c6f5e83794b2755846526e9 Mon Sep 17 00:00:00 2001
From: Oskar Viljasaar <oskar.viljasaar@gmail.com>
Date: Fri, 21 Sep 2018 16:36:50 +0200
Subject: [PATCH 4/6] crypto: caam: Consolidate secure memory handling at CAAM
 probe

Existing code has two paths checking for secure memory, depending on
the presence of a SECO tile. Make it a bit more robust:
- Check only once for secure memory, before SECO and non-SECO specific
  probing.
- Use exclusively device managed IO mappings for secure memory.

Cc: Oskar Viljasaar <oviljasaar@witekio.com>
Signed-off-by: Oskar Viljasaar <oskar.viljasaar@gmail.com>
---
 drivers/crypto/caam/ctrl.c | 54 ++++++++++++++++----------------------
 1 file changed, 23 insertions(+), 31 deletions(-)

diff --git a/drivers/crypto/caam/ctrl.c b/drivers/crypto/caam/ctrl.c
index 5baa04c6e159..aca37d2062f9 100644
--- a/drivers/crypto/caam/ctrl.c
+++ b/drivers/crypto/caam/ctrl.c
@@ -505,7 +505,6 @@ static int read_first_jr_index(struct caam_drv_private *ctrlpriv)
 static int probe_w_seco(struct caam_drv_private *ctrlpriv)
 {
 	int ret = 0;
-	struct device_node *np;
 	u32 idx, status;
 
 	ctrlpriv->has_seco = true;
@@ -540,14 +539,6 @@ static int probe_w_seco(struct caam_drv_private *ctrlpriv)
 
 	detect_era(ctrlpriv);
 
-	/* Get CAAM-SM node and of_iomap() and save */
-	np = of_find_compatible_node(NULL, NULL, "fsl,imx6q-caam-sm");
-	if (!np) {
-		dev_warn(ctrlpriv->dev, "No CAAM-SM node found!\n");
-		return -ENODEV;
-	}
-
-	ctrlpriv->sm_base = of_iomap(np, 0);
 	ctrlpriv->sm_size = 0x3fff;
 
 	/* Can't enable DECO WD and LPs those are in MCR */
@@ -617,6 +608,29 @@ static int caam_probe(struct platform_device *pdev)
 	}
 	ctrlpriv->ctrl = (struct caam_ctrl __force *)ctrl;
 
+	/* Get CAAM-SM node and of_iomap() and save */
+	np = of_find_compatible_node(NULL, NULL, "fsl,imx6q-caam-sm");
+	if (!np) {
+		ret = -ENODEV;
+		goto iounmap_ctrl;
+	}
+
+	/* Get CAAM SM registers base address from device tree */
+	ret = of_address_to_resource(np, 0, &res_regs);
+	if (ret) {
+		dev_err(dev, "failed to retrieve registers base from device tree\n");
+		ret = -ENODEV;
+		goto iounmap_ctrl;
+	}
+
+	ctrlpriv->sm_phy = res_regs.start;
+	ctrlpriv->sm_base = devm_ioremap_resource(dev, &res_regs);
+
+	if (IS_ERR(ctrlpriv->sm_base)) {
+		ret = PTR_ERR(ctrlpriv->sm_base);
+		goto iounmap_ctrl;
+	}
+
 	if (of_machine_is_compatible("fsl,imx8qm") ||
 		 of_machine_is_compatible("fsl,imx8qxp")) {
 		ret = probe_w_seco(ctrlpriv);
@@ -651,28 +665,6 @@ static int caam_probe(struct platform_device *pdev)
 
 	detect_era(ctrlpriv);
 
-	/* Get CAAM-SM node and of_iomap() and save */
-	np = of_find_compatible_node(NULL, NULL, "fsl,imx6q-caam-sm");
-	if (!np) {
-		ret = -ENODEV;
-		goto disable_clocks;
-	}
-
-	/* Get CAAM SM registers base address from device tree */
-	ret = of_address_to_resource(np, 0, &res_regs);
-	if (ret) {
-		dev_err(dev, "failed to retrieve registers base from device tree\n");
-		ret = -ENODEV;
-		goto disable_clocks;
-	}
-
-	ctrlpriv->sm_phy = res_regs.start;
-	ctrlpriv->sm_base = devm_ioremap_resource(dev, &res_regs);
-	if (IS_ERR(ctrlpriv->sm_base)) {
-		ret = PTR_ERR(ctrlpriv->sm_base);
-		goto disable_clocks;
-	}
-
 	if (!of_machine_is_compatible("fsl,imx8mq") &&
 	     !of_machine_is_compatible("fsl,imx8mm") &&
 	     !of_machine_is_compatible("fsl,imx8qm") &&
-- 
2.17.1

