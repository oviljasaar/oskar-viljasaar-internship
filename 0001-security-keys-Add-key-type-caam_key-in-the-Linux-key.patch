From 2739d1c55c64b077f661b2ff7ff291524df93825 Mon Sep 17 00:00:00 2001
From: Marius Bagassien <mbagassien@witekio.com>
Date: Fri, 11 Aug 2017 16:55:39 +0200
Subject: [PATCH] security: keys: Add key type "caam_key" in the Linux key
 retention service

caam_key interfaces with a CAAM present in NXP i.MX-based systems. Allow
for encapsulation and decapsulation of red and black keys and blobs
derived thereof. A blob can be decrypted only by the same system having
created the blob beforehand, because the blob contains data derived from
the system's internal state.

To load a given unencrypted key or secret:
$ keyctl add caam_key redkey "crypt key=`cat key` len=32 \
    keymod=`cat keymod` color=red mode=ecb" @u
  Where:
- "key" is the key to be loaded;
- "len" is the key's length in bytes;
- "keymod" is a key modifier used to encrypt the key, encoded in hex;
- "color" is either "red" or "black", depending on whether we want to get
  the original secret back or let the CAAM operate on it in its encrypted
  form;
- "mode" is the encryption mode. "ecb" and "ccm" are supported for black
  keys. Only "ecb" is supported for red keys.

To load a given blob:
$ keyctl add caam_key blackblob "load key=`cat blob` len=32 \
    keymod=`cat keymod` color=black mode=ecb enc=hex" @u
  Where:
- "key" is the blob data;
- "len" is the unblobbed key's length (the blob is 48 bytes longer);
The rest of the arguments follow the model described above.

Exporting blobs from CAAM is not supported at the moment.

Signed-off-by: Oskar Viljasaar <oviljasaar@witekio.com>
Signed-off-by: Marius Bagassien <mbagassien@witekio.com>
---
 drivers/crypto/caam/Kconfig  |   3 +-
 include/keys/caam-key-type.h | 194 ++++++++++++
 security/keys/Kconfig        |  13 +
 security/keys/Makefile       |   1 +
 security/keys/caam_key.c     | 575 +++++++++++++++++++++++++++++++++++
 5 files changed, 785 insertions(+), 1 deletion(-)
 create mode 100644 include/keys/caam-key-type.h
 create mode 100644 security/keys/caam_key.c

diff --git a/drivers/crypto/caam/Kconfig b/drivers/crypto/caam/Kconfig
index e3803d142e21..5553f3dc48cb 100644
--- a/drivers/crypto/caam/Kconfig
+++ b/drivers/crypto/caam/Kconfig
@@ -1,6 +1,6 @@
 config CRYPTO_DEV_FSL_CAAM
 	tristate "Freescale CAAM-Multicore driver backend"
-	depends on FSL_SOC || ARCH_MXC || ARCH_LAYERSCAPE
+	depends on FSL_SOC || ARCH_MXC || ARCH_LAYERSCAPE || ARCH_MXC_ARM64
 	help
 	  Enables the driver module for Freescale's Cryptographic Accelerator
 	  and Assurance Module (CAAM), also known as the SEC version 4 (SEC4).
@@ -140,6 +140,7 @@ config CRYPTO_DEV_FSL_CAAM_RNG_TEST
 	  just before the RNG is registered with the hw_random API.
 
 config CRYPTO_DEV_FSL_CAAM_SM
+       depends on CRYPTO_DEV_FSL_CAAM
 	tristate "CAAM Secure Memory / Keystore API (EXPERIMENTAL)"
 	default n
 	help
diff --git a/include/keys/caam-key-type.h b/include/keys/caam-key-type.h
new file mode 100644
index 000000000000..732c6a52a26a
--- /dev/null
+++ b/include/keys/caam-key-type.h
@@ -0,0 +1,194 @@
+/*
+ * Copyright (C) 2017-2018 Witekio
+ *
+ * Authors:
+ * Marius Bagassien <mbagassien@witekio.com>
+ * Oskar Viljasaar <oviljasaar@witekio.com>
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License as published by
+ * the Free Software Foundation, version 2 of the License.
+ */
+
+#ifndef _KEYS_CAAM_TYPE_H
+#define _KEYS_CAAM_TYPE_H
+
+#include <linux/key.h>
+#include <linux/rcupdate.h>
+
+/* Red and black keys have the same size limits. */
+#define MIN_KEY_LEN		32
+#define MAX_KEY_LEN		128
+
+#define MAX_BLOB_LEN		320
+
+#define MAX_KEYMOD_LEN		128
+
+#define RED_KEY			0
+#define BLACK_KEY		1
+#define KEY_HEX			0
+#define KEY_BASE64		1
+
+#define BLOB_TO_KEY(x) (x - 48)
+
+/*
+ * Round a key size up to an AES blocksize boundary so to allow for
+ * padding out to a full block
+ */
+#define AES_BLOCK_PAD(x) ((x % 16) ? ((x >> 4) + 1) << 4 : x)
+
+#define BYTE_BLOCK_PAD(x) ((x % 8) ? ((x >> 3) + 1) << 3 : x)
+
+/* Blobs are 48 bytes longer than unblobbed keys */
+#define BLOB_SIZE(x) (x + 48)
+
+/* Define key encryption/covering options */
+#define KEY_COVER_ECB 0
+#define KEY_COVER_CCM 1
+
+struct device *sm_get_device(void);
+void   sm_release(void);
+int    sm_establish_keystore(struct device *dev, u32 unit);
+void   sm_release_keystore(struct device *dev, u32 unit);
+u32 sm_detect_keystore_units(struct device *dev);
+
+/* CAAM Kestore API */
+extern u32 sm_detect_keystore_units(struct device *dev);
+
+extern int sm_keystore_slot_alloc(struct device *dev, u32 unit, u32 size,
+				  u32 *slot);
+extern int sm_keystore_slot_dealloc(struct device *dev, u32 unit, u32 slot);
+extern int sm_keystore_slot_load(struct device *dev, u32 unit, u32 slot,
+				 const u8 *key_data, u32 key_length);
+extern int sm_keystore_slot_read(struct device *dev, u32 unit, u32 slot,
+				 u32 key_length, u8 *key_data);
+extern int sm_keystore_slot_export(struct device *dev, u32 unit, u32 slot,
+				   u8 keycolor, u8 keyauth, u8 *outbuf,
+				   u16 keylen, u8 *keymod);
+extern int sm_keystore_slot_import(struct device *dev, u32 unit, u32 slot,
+				   u8 keycolor, u8 keyauth, u8 *inbuf,
+				   u16 keylen, u8 *keymod);
+
+extern int sm_keystore_cover_key(struct device *dev, u32 unit, u32 slot,
+				 u16 key_length, u8 keyauth);
+
+/* Defines whether a payload is a secret (red) or an encrypted key (black). */
+enum caam_color {
+	red = RED_KEY,
+	black = BLACK_KEY,
+};
+
+/* Defines encryption mode for black keys, unused for red keys. */
+enum caam_mode {
+	ecb = KEY_COVER_ECB,
+	ccm = KEY_COVER_CCM,
+};
+
+/* A generic CAAM blob. */
+struct caam_blob {
+	uint8_t payload[MAX_BLOB_LEN];
+	uint32_t len;
+};
+
+
+/* Key modifier. */
+struct caam_keymod {
+	uint8_t payload[MAX_KEYMOD_LEN];
+	uint32_t len;
+};
+
+/* CAAM key attributes. */
+struct caam_key_attrs {
+	uint32_t len;
+	enum caam_color color;
+	enum caam_mode mode;
+	/* Slot used in secure memory. */
+	uint32_t slot;
+};
+
+/* A generic CAAM key. */
+struct caam_key {
+	struct rcu_head rcu;
+	struct caam_key_attrs attrs;
+};
+
+/*
+ * These structures are used for ioctls and tasks internal to the module.
+ */
+/* Blacken a red key. */
+struct caam_red2black {
+	/* in: the red key's payload, to be zeroized after copying into secure
+	 * memory.
+	 */
+	uint8_t payload[MAX_KEY_LEN];
+	/* in: attributes pertaining to the payload. */
+	struct caam_key_attrs red;
+	/* out: attributes pertaining to the blackened key. */
+	struct caam_key_attrs black;
+};
+
+/* Encapsulate a key with
+ * - a given color
+ * - a given encryption mode
+ * - a given key modifier
+ */
+struct caam_key2blob {
+	/* in: the key's payload, to be zeroized after copying it into secure
+	 * memory.
+	 */
+	uint8_t payload[MAX_KEY_LEN];
+	/* in: attributes pertaining to the payload. */
+	struct caam_key_attrs attrs;
+	/* in: the key modifier. */
+	struct caam_keymod mod;
+};
+
+/* Likewise, decapsulate a blob. */
+struct caam_blob2key {
+	/* in: the blob payload and its length. */
+	struct caam_blob blob;
+	/* in: the derived key's attributes also pertaining to the blob. */
+	struct caam_key_attrs attrs;
+	/* in: the key modifier. */
+	struct caam_keymod mod;
+	/* out: the resulting decapsulated key. */
+	uint8_t decapsulated_key[MAX_KEY_LEN];
+};
+
+/*
+ * Argument passed in ioctls, the right structure used in the union is
+ * determined with the ioctl number, one per union member..
+ */
+struct caam_ioctl {
+	union {
+		struct caam_red2black r2b;
+		struct caam_key2blob  k2b;
+		struct caam_blob2key  b2k;
+	} u;
+};
+
+/* Structure used to store the given datablob's parsed items. */
+struct  caam_key_options {
+	enum caam_mode mode;
+	enum caam_color color;
+
+	/* Preparsed data: this might either be a blob, a red key or a black
+	 * key. Also it might be encoded either in hex or base64. The binary
+	 * length is given with the "len" argument in the datablob, useful to
+	 * verify that we've got a valid key payload.
+	 */
+	struct caam_blob data;
+
+	/* This is used to temporarily store an untreated key modifier, we can
+	 * rely on strlen() to get its length.
+	 */
+	struct caam_keymod mod;
+};
+
+static inline void dump_key(struct caam_key *p)
+{
+	pr_debug("Key len %u color %d mode %d slot %u\n",
+		 p->attrs.len, p->attrs.color, p->attrs.mode, p->attrs.slot);
+}
+
+#endif /* _KEYS_CAAM_TYPE_H */
diff --git a/security/keys/Kconfig b/security/keys/Kconfig
index f2980084a38f..be8ac99e0b97 100644
--- a/security/keys/Kconfig
+++ b/security/keys/Kconfig
@@ -99,3 +99,16 @@ config KEY_DH_OPERATIONS
 	 in the kernel.
 
 	 If you are unsure as to whether this is required, answer N.
+
+config CAAM_KEYS
+	tristate "CAAM_KEYS"
+	depends on KEYS
+	select CRYPTO_DEV_FSL_CAAM_SM
+	help
+	  This option provides support for creating, sealing, and unsealing
+	  keys in the kernel. CAAM keys are random number symmetric keys,
+	  generated and sealed by the CAAM (Cryptographic Acceleration and
+	  Assurance Module). The CAAM only unseals the keys, userspace will
+	  only ever see encrypted blobs if instantiated so.
+
+	  If you are unsure as to whether this is required, answer N.
diff --git a/security/keys/Makefile b/security/keys/Makefile
index 1fd4a16e6daf..897d32671ba1 100644
--- a/security/keys/Makefile
+++ b/security/keys/Makefile
@@ -26,4 +26,5 @@ obj-$(CONFIG_KEY_DH_OPERATIONS) += dh.o
 #
 obj-$(CONFIG_BIG_KEYS) += big_key.o
 obj-$(CONFIG_TRUSTED_KEYS) += trusted.o
+obj-$(CONFIG_CAAM_KEYS) += caam_key.o
 obj-$(CONFIG_ENCRYPTED_KEYS) += encrypted-keys/
diff --git a/security/keys/caam_key.c b/security/keys/caam_key.c
new file mode 100644
index 000000000000..2e2b03210f41
--- /dev/null
+++ b/security/keys/caam_key.c
@@ -0,0 +1,575 @@
+/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */
+/*
+ * caam_pkey: userspace interface to the CAAM secure store
+ * Somewhat inspired by the pkey API.
+ *
+ * Copyright (C) 2017-2018 Witekio
+ * Author: Oskar Viljasaar <oviljasaar@witekio.com>
+ */
+
+#include <linux/key-type.h>
+#include <linux/module.h>
+#include <linux/init.h>
+#include <linux/parser.h>
+#include <linux/slab.h>
+#include <linux/fs.h>
+#include <linux/uaccess.h>
+#include <linux/device.h>
+
+#include <keys/caam-key-type.h>
+
+MODULE_LICENSE("GPL");
+MODULE_AUTHOR("Oskar Viljasaar <oviljasaar@witekio.com>");
+MODULE_DESCRIPTION("CAAM-SM key interface");
+
+static struct device *sm_device;
+static uint32_t keystore_unit;
+
+enum {
+	Opt_err = -1,
+	Opt_crypt,
+	Opt_load,
+	Opt_mode,
+	Opt_color,
+	Opt_len,
+	Opt_key,
+	Opt_keymod
+};
+
+static const match_table_t key_tokens = {
+	{Opt_crypt, "crypt"},
+	{Opt_load, "load"},
+	{Opt_mode, "mode=%s"},
+	{Opt_color, "color=%s"},
+	{Opt_len, "len=%d"},
+	{Opt_key, "key=%s"},
+	{Opt_keymod, "keymod=%s"},
+	{Opt_err, NULL}
+};
+
+
+static const int caam_hex(const char *str, u8 *result, uint32_t *count)
+{
+	size_t slen = strlen(str);
+	size_t ulen;
+
+	if (!slen || slen % 2)
+		return -EILSEQ;
+
+	ulen = slen / 2;
+
+	if (hex2bin(result, str, ulen))
+		return -EILSEQ;
+
+	*count = ulen;
+
+	return 0;
+}
+
+/*
+ * Have the CAAM seal(encrypt) the red attrs.
+ */
+int caam_red_to_black(struct caam_red2black *r)
+{
+	int ret = 0;
+
+	/* We support keys up to 256 bits (32 bytes) */
+	if ((r->red.len > MAX_KEY_LEN) ||
+	    (r->red.len & 1)) {
+		pr_err("wrong key size (%d)\n", r->red.len);
+		return -EILSEQ;
+	}
+
+	if (sm_keystore_slot_load(sm_device, keystore_unit, r->red.slot,
+				  r->payload, r->red.len)) {
+		pr_err("Could not load red key in secure memory\n");
+		ret = -ENODEV;
+		goto out;
+	}
+
+	/* Zero out the clear payload */
+	memset(r->payload, 0, MAX_KEY_LEN);
+
+	if (sm_keystore_cover_key(sm_device, keystore_unit, r->red.slot,
+				  r->red.len, KEY_COVER_ECB)) {
+		pr_err("Could not cover key\n");
+		ret = -ENODEV;
+		goto out;
+	}
+
+	switch (r->red.mode) {
+	case KEY_COVER_ECB:
+		r->black.len = AES_BLOCK_PAD(r->red.len);
+		break;
+	case KEY_COVER_CCM:
+		r->black.len = BYTE_BLOCK_PAD(r->red.len) + 12;
+		break;
+	default:
+		pr_err("Mode error\n");
+		break;
+	}
+
+	r->black.mode = r->red.mode;
+	r->black.color = BLACK_KEY;
+	r->black.slot = r->red.slot;
+
+out:
+	return ret;
+}
+
+static size_t caam_blob_to_key(struct caam_blob2key *r)
+{
+	int ret = 0;
+
+	if (r->attrs.len > MAX_KEY_LEN || r->attrs.len < MIN_KEY_LEN) {
+		pr_err("Invalid decapsulated key size (got %u)\n",
+		       r->attrs.len);
+		return -EINVAL;
+	}
+
+	if (r->blob.len > MAX_BLOB_LEN) {
+		ret = -EFBIG;
+		return ret;
+	}
+
+	ret = sm_keystore_slot_load(sm_device, keystore_unit,
+				    r->attrs.slot, r->blob.payload,
+				    r->blob.len);
+	if (ret) {
+		pr_err("Could not load key in slot\n");
+		goto out;
+	}
+	r->attrs.len = BLOB_TO_KEY(r->attrs.len);
+
+	ret = sm_keystore_slot_import(sm_device, keystore_unit, r->attrs.slot,
+				      r->attrs.color, r->attrs.mode,
+				      r->blob.payload, r->attrs.len,
+				      r->mod.payload);
+	if (ret) {
+		pr_err("can't decapsulate %d-bit key blob (%d)\n",
+			r->attrs.len << 3, ret);
+		goto out;
+	}
+
+out:
+	return ret;
+}
+
+/* Parse options. This includes copying the key modifier and the blob/attrs. */
+static int getoptions(char *c, struct caam_key_options *o)
+{
+	substring_t args[MAX_OPT_ARGS];
+	char arg[MAX_BLOB_LEN];
+	char *p = c;
+	int token;
+	int ret = 0;
+
+	while ((p = strsep(&c, " \t"))) {
+		if (*p == '\0' || *p == ' ' || *p == '\t')
+			continue;
+		token = match_token(p, key_tokens, args);
+
+		switch (token) {
+		case Opt_mode:
+		case Opt_color:
+		case Opt_key:
+		case Opt_keymod:
+			/* Copy string arguments for type safety */
+			if (match_strlcpy(arg, args, MAX_BLOB_LEN) == 0)
+				return -EINVAL;
+			break;
+		default:
+			/* Do nothing */
+			break;
+		}
+
+		switch (token) {
+		case Opt_mode:
+			if (!strcmp(arg, "ecb"))
+				o->mode =  KEY_COVER_ECB;
+			if (!strcmp(arg, "ccm"))
+				o->mode = KEY_COVER_CCM;
+			break;
+		case Opt_color:
+			if (!strcmp(arg, "red"))
+				o->color = RED_KEY;
+			if (!strcmp(arg, "black"))
+				o->color = BLACK_KEY;
+			break;
+		case Opt_len:
+			/*
+			 * This is the key/secret's length, given to be sure
+			 * of the payload's size.
+			 */
+			ret = match_int(args, &o->data.len);
+			if (ret < 0)
+				return ret;
+			break;
+		case Opt_key:
+			/*
+			 * We'd parse the key right away but it's possible we
+			 * haven't got far enough in parsing to know the
+			 * encoding. memcpy() instead the data and decode it
+			 * afterwards.
+			 */
+			memcpy(o->data.payload, arg, strlen(arg));
+			break;
+		case Opt_keymod:
+			/* Parse the key modifier. */
+			caam_hex(arg, o->mod.payload, &o->mod.len);
+			if (ret < 0)
+				return ret;
+			break;
+		case Opt_crypt:
+		case Opt_load:
+			break;
+		default:
+			pr_err("Invalid option %s\n", p);
+			return -EINVAL;
+		}
+	}
+	return 0;
+}
+
+/*
+ * Parse the keyctl data and fill in the payload and options structures.
+ *
+ * On success returns 0, otherwise -EINVAL.
+ */
+static int datablob_parse(char *datablob, struct caam_ioctl *w,
+			  struct caam_key_options *o)
+{
+	substring_t args[MAX_OPT_ARGS];
+	int ret = -EINVAL;
+	int key_cmd;
+
+	struct caam_key2blob *k2b = &w->u.k2b;
+	struct caam_blob2key *b2k = &w->u.b2k;
+
+	ret = getoptions(datablob, o);
+
+	key_cmd = match_token(datablob, key_tokens, args);
+
+	switch (key_cmd) {
+
+	case Opt_crypt:
+		/* Parse the key with the given encoding. */
+		ret = caam_hex(o->data.payload, k2b->payload, &k2b->attrs.len);
+		if (ret < 0)
+			return ret;
+
+		k2b->attrs.len = o->data.len;
+		k2b->attrs.color = o->color;
+		k2b->attrs.mode = o->mode;
+
+		ret = Opt_crypt;
+		break;
+	case Opt_load:
+		/* Decode the blob. */
+		ret = caam_hex(o->data.payload, b2k->blob.payload,
+			       &b2k->blob.len);
+		if (ret < 0)
+			return ret;
+
+		/* Store key attributes. */
+		b2k->attrs.len = o->data.len;
+		b2k->attrs.color = o->color;
+		b2k->attrs.mode = o->mode;
+
+		memcpy(b2k->mod.payload, o->mod.payload, o->mod.len);
+		b2k->mod.len = o->mod.len;
+
+		ret = Opt_load;
+		break;
+	case Opt_err:
+	default:
+		return -EINVAL;
+	}
+
+	return ret;
+}
+
+/* Allocate key payload */
+static struct caam_key *caam_key_alloc(struct key *key)
+{
+	struct caam_key *p = NULL;
+	int ret;
+
+	ret = key_payload_reserve(key, sizeof(*p));
+	if (ret < 0)
+		return p;
+	p = kzalloc(sizeof(*p), GFP_KERNEL);
+
+	return p;
+}
+
+/*
+ * caam_key_instantiate - create a new caam_key
+ *
+ * Either:
+ * - instantiate a black key from a plaintext key
+ * - unseal an existing blob and instantiate a key
+ *
+ * Does not create a new blob, only returns the inner key when read.
+ *
+ * Returns 0 on success. Otherwise returns retno.
+ */
+static int caam_key_instantiate(struct key *key,
+				struct key_preparsed_payload *prep)
+
+{
+	unsigned int cmd;
+
+	struct caam_key_options *options = NULL;
+	size_t datalen = prep->datalen;
+	char *datablob; /*Data fetched from command line */
+	struct caam_ioctl *work;
+	struct caam_blob2key *b2k = NULL;
+	struct caam_key2blob *k2b = NULL;
+	struct caam_key *ckey = NULL;
+	int ret = 0;
+
+	if (datalen <= 0 || datalen > 32767 || !prep->data)
+		return -EINVAL;
+
+	datablob = kzalloc(datalen, GFP_KERNEL);
+	if (!datablob) {
+		ret = -ENOMEM;
+		return ret;
+	}
+
+	work = kzalloc(sizeof(struct caam_ioctl), GFP_KERNEL | GFP_DMA);
+	if (!work) {
+		ret = -ENOMEM;
+		goto out;
+	}
+
+	memcpy(datablob, prep->data, datalen);
+
+	ckey = caam_key_alloc(key);
+	if (!ckey) {
+		ret = -ENOMEM;
+		goto out;
+	}
+
+	options = kzalloc(sizeof(struct caam_key_options), GFP_KERNEL);
+	if (options == NULL) {
+		ret = -ENOMEM;
+		goto out;
+	}
+
+	cmd = datablob_parse(datablob, work, options);
+	if (cmd < 0) {
+		ret = cmd;
+		goto out_options;
+	}
+
+	switch (cmd) {
+	case Opt_load:
+		b2k = &work->u.b2k;
+
+		ret = sm_keystore_slot_alloc(sm_device, keystore_unit,
+					    b2k->attrs.len, &b2k->attrs.slot);
+		if (ret)
+			goto out_options;
+
+		ret = caam_blob_to_key(b2k);
+		if (ret < 0) {
+			pr_err("caam_key: blob decode failed (%d)\n", ret);
+			goto out_options;
+		}
+
+		memcpy(&ckey->attrs, &b2k->attrs,
+		       sizeof(struct caam_key_attrs));
+		break;
+	case Opt_crypt:
+		/*
+		 * Use the k2b structure because it lends itself well to what we
+		 * want to do, even though we won't be creating a blob.
+		 */
+		k2b = &work->u.k2b;
+		ret = sm_keystore_slot_alloc(sm_device, keystore_unit,
+					     k2b->attrs.len, &k2b->attrs.slot);
+
+		if (ret) {
+			pr_err("Could not allocate slot\n");
+			goto out_options;
+		}
+		ret = sm_keystore_slot_load(sm_device, keystore_unit,
+					    k2b->attrs.slot, k2b->payload,
+					    k2b->attrs.len);
+		if (ret) {
+			pr_err("Could not load key in slot\n");
+			goto out_options;
+		}
+
+		/* Black keys are encrypted, so cover them in secure memory. */
+		if (k2b->attrs.color == BLACK_KEY) {
+			ret = sm_keystore_cover_key(sm_device, keystore_unit,
+						    k2b->attrs.slot,
+						    k2b->attrs.len,
+						    k2b->attrs.mode);
+			if (ret) {
+				pr_err("Could not cover key\n");
+				goto out_options;
+			}
+
+			switch (k2b->attrs.mode) {
+			case KEY_COVER_ECB:
+				k2b->attrs.len = AES_BLOCK_PAD(k2b->attrs.len);
+				break;
+			case KEY_COVER_CCM:
+				k2b->attrs.len =
+					BYTE_BLOCK_PAD(k2b->attrs.len) + 12;
+				break;
+			default:
+				pr_err("Wrong mode\n");
+				break;
+			}
+		}
+
+		memcpy(&ckey->attrs, &k2b->attrs,
+		       sizeof(struct caam_key_attrs));
+		break;
+	default:
+		ret = -EINVAL;
+		goto out_options;
+	}
+#ifdef SM_DEBUG
+	dump_payload(payload);
+	dump_options(options);
+#endif
+ out_options:
+	kfree(options);
+
+	if (!ret)
+		rcu_assign_keypointer(key, ckey);
+	else
+		kfree(ckey);
+
+ out:
+	kfree(datablob);
+	return ret;
+}
+
+static int caam_key_update(struct key *key, struct key_preparsed_payload *prep)
+{
+	return 0;
+}
+
+/* Clear the data from a key */
+static void caam_key_destroy(struct key *key)
+{
+	struct caam_key *p = (struct caam_key *) key->payload.data;
+
+	if (!p)
+		return;
+
+	sm_keystore_slot_dealloc(sm_device, keystore_unit, p->attrs.slot);
+
+	kfree(rcu_dereference_key(key));
+}
+
+/* Describe a key */
+static void caam_key_describe(const struct key *key, struct seq_file *p)
+{
+}
+
+/* Copy the black blob to userspace. */
+static long caam_key_read(const struct key *key,
+			  char __user *buffer,
+			  size_t buflen)
+{
+	int ret;
+	struct caam_key *p;
+	char sec_buf[MAX_KEY_LEN];
+	char *ascii_buf;
+	char *bufp;
+	int i;
+
+	p = rcu_dereference_key(key);
+	if (!p)
+		return -EINVAL;
+
+	if (!buffer || buflen <= 0)
+		return 2*p->attrs.len;
+
+	ret = sm_keystore_slot_read(sm_device, keystore_unit, p->attrs.slot,
+				    p->attrs.len, sec_buf);
+	if (ret) {
+		pr_err("Could not read key from secure memory.\n");
+		return ret;
+	}
+
+	ascii_buf = kzalloc(2 * p->attrs.len, GFP_KERNEL);
+	if (!ascii_buf)
+		return -ENOMEM;
+
+	bufp = ascii_buf;
+	for (i = 0; i < p->attrs.len; i++)
+		bufp = hex_byte_pack(bufp, sec_buf[i]);
+
+	if ((copy_to_user(buffer, ascii_buf, 2 * p->attrs.len)) != 0) {
+		kfree(ascii_buf);
+		return -EFAULT;
+	}
+
+	kfree(ascii_buf);
+
+	return 2*p->attrs.len;
+}
+
+
+static const struct file_operations caam_pkey_ops = {
+	.owner = THIS_MODULE,
+	.open = nonseekable_open,
+	.llseek = no_llseek,
+};
+
+struct key_type key_type_caam_key = {
+	.name			= "caam_key",
+	.instantiate		= caam_key_instantiate,
+	.update			= caam_key_update,
+	.destroy		= caam_key_destroy,
+	.describe		= caam_key_describe,
+	.read			= caam_key_read,
+};
+
+static int __init caam_pkey_init(void)
+{
+	sm_device = sm_get_device();
+
+	if (!sm_device)
+		return -ENODEV;
+
+	/* Assign ourselves a keystore unit */
+	keystore_unit = sm_detect_keystore_units(sm_device);
+	if (!keystore_unit) {
+		pr_err("no keystore keystore_unit available\n");
+		return -ENODEV;
+	}
+
+	/*
+	 * IMX6 bootloader stores some stuff in unit 0, so let's
+	 * use 1 or above
+	 */
+	if (keystore_unit < 2) {
+		pr_err("insufficient keystore keystore_unit\n");
+		return -ENODEV;
+	}
+
+	keystore_unit = 1;
+	sm_establish_keystore(sm_device, keystore_unit);
+
+	return register_key_type(&key_type_caam_key);
+}
+
+static void __exit caam_pkey_exit(void)
+{
+	unregister_key_type(&key_type_caam_key);
+	sm_release_keystore(sm_device, keystore_unit);
+	sm_release();
+}
+
+late_initcall(caam_pkey_init);
+module_exit(caam_pkey_exit);
-- 
2.17.1

